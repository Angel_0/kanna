const { RichEmbed } = require('discord.js') 
const talkedRecently = new Set();

exports.run = async (client, message, args, level) => {
  let embed = new RichEmbed()
  if(args.length !== 0) {
    if (message.mentions.users.first() !== undefined) {
      let user = message.mentions.users.first()  
      embed.setTitle(`Avatar of ${user.username}`)
      embed.setDescription(`[Avatar link](${user.displayAvatarURL})`)
      embed.setImage(user.displayAvatarURL)
      message.channel.send(embed)
    } else {
      if (args[0].match(/\d{17,18}/gm)) {
        let user = message.guild.members.find(m => m.id == args[0]).user
        embed.setTitle(`Avatar of ${user.username}`)
        embed.setDescription(`[Avatar link](${user.displayAvatarURL})`)
        embed.setImage(user.displayAvatarURL)
        message.channel.send(embed)
      } else {  
        let user = message.guild.members.find(mem => mem.displayName.toLowerCase().includes(args.join(' ').toLowerCase())).user
        embed.setTitle(`Avatar of ${user.username}`)
        embed.setDescription(`[Avatar link](${user.displayAvatarURL})`)
        embed.setImage(user.displayAvatarURL)
        message.channel.send(embed)
      }
    }
  } else {
    let user = message.author
    embed.setTitle(`Avatar of ${user.username}`)
    embed.setDescription(`[Avatar link](${user.displayAvatarURL})`)
    embed.setImage(user.displayAvatarURL)
    message.channel.send(embed)
  }
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "avatar",
  category: "Utility",
  description: "Shows you your profile picture, or the one of the provided member.",
  usage: "avatar [username|id|@user]"
}; 
